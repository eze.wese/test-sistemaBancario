
public class Cliente {
	private String nombreYApellido;
	private int dni;
	private boolean monotributista;
	Cliente (String n, int d, boolean m){
		nombreYApellido=n;
		dni=d;
		monotributista=m;
	}
	
	public boolean getMonotributista() {
		return monotributista;
	}
	
	public int getDni() {
		return dni;
	}
	public String getNmobreApellido() {
		return nombreYApellido;
	}
}
