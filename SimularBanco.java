import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SimularBanco {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BufferedReader entrada = new BufferedReader(new InputStreamReader(System.in));
		try {
			
			System.out.println ("Ingrese string para iniciar primera parte: ");
			String opcion = entrada.readLine();
			Banco Banco = new Banco();
			Cliente C1 = new Cliente("Luis Caballero", 22, true);
			Cliente C2 = new Cliente("Jose Perez", 33, false);
			Cliente C3 = new Cliente("Carlos Lorenzo", 44, true);
			Banco.AgregarCliente(C1);
			Banco.AgregarCliente(C2);
			Banco.AgregarCliente(C3);
			Banco.AsignarCuenta(22);
			Banco.AsignarCuenta(22);
			Banco.AsignarCuenta(44);
			Banco.AsignarCuenta(33);
			Banco.ListarCliente(22);
			Banco.ListarCliente(33);
			Banco.ListarCliente(44);
			
			System.out.println ("Ingrese string para iniciar segunda parte: ");
			opcion = entrada.readLine();
			
			Banco.Depositar(22, 1, 100);
			Banco.Depositar(33, 1, 100);
			Banco.RetirarDinero(22, 1, 50);
			Banco.ListarCliente(22);
			Banco.ListarCliente(33);
			Banco.ListarCliente(44);
			
			System.out.println ("Ingrese string para iniciar tercera parte: ");
			opcion = entrada.readLine();
			
			Banco.TransferirDinero(22, 50, 1, 2);
			Banco.TransferirDinero(22, 10, 1, 3);
			Banco.ListarCliente(22);
			Banco.ListarCliente(33);
			Banco.ListarCliente(44);
			
			System.out.println ("Ingrese string para iniciar cuarta parte: ");
			opcion = entrada.readLine();
			
			Banco.TransferirDinero(33, 50, 1, 2);
			Banco.TransferirDinero(22, 100, 1, 2);
			Banco.ListarCliente(22);
			Banco.ListarCliente(33);
			Banco.ListarCliente(44);
			
			System.out.println ("Ingrese string para iniciar quinta parte: ");
			opcion = entrada.readLine();
			
			Banco.CerrarCuenta(22, 1);
			Banco.TransferirDinero(44, 4, 3, 4);
			Banco.ListarCliente(22);
			Banco.ListarCliente(33);
			Banco.ListarCliente(44);
			
			System.out.println ("Ingrese string para iniciar sexta parte: ");
			opcion = entrada.readLine();
			
			Banco.CerrarCuenta(22, 2);
			Banco.CerrarCuenta(33, 4);
			Banco.CerrarCuenta(44, 3);
			Banco.ListarCliente(22);
			Banco.ListarCliente(33);
			Banco.ListarCliente(44);
		}
		catch (Exception exc ) {
			System.out.println( exc );
			}
		}
		
	
		}
