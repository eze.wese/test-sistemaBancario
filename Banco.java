
public class Banco {
	final static int MAX=1000;
	private Cliente arregloclientes[];
	private Cuenta arreglocuentas[];
	Banco() {
		arregloclientes = new Cliente[MAX];
		arreglocuentas = new Cuenta[MAX];
		for (int i=0; i<MAX; i++) {
			arreglocuentas[i] = new Cuenta(i+1);
		}
	}
	
	public int ver() {
		return arreglocuentas[7-1].getNroCuenta();
	}
	
	public void AgregarCliente(Cliente x) {
		int i=0;
		while(arregloclientes[i]!=null && i<arregloclientes.length-1) {
			i++;	
		}
		if(arregloclientes[i]==null) {
			arregloclientes[i]=x;
		}
	}
	
	public void AsignarCuenta(int dni) {
		int i=0;
		while(arreglocuentas[i].getCliente()!=null && i<arreglocuentas.length-1) {
			i++;	
		}
		if(arreglocuentas[i].getCliente()==null) {
			Cliente c = BuscarClienteXdni(dni);
			if(c==null) {
				System.out.println("Cliente no existe");
			}
			else {
				arreglocuentas[i].setCliente(c);
			}
		}
		else {
			System.out.println("No hay cuentas disponibles");
		}
	}
	
	private Cliente BuscarClienteXdni(int dni) {
		int i=0;
		Cliente c=null;
		while(i<arregloclientes.length && arregloclientes[i]!=null && arregloclientes[i].getDni()!=dni) {
			i++;
		}
		if (arregloclientes[i]==null) {
			c=null;
		}
		else if (arregloclientes[i].getDni()==dni) {
			c=arregloclientes[i];
		}
		return c;
	}
	
	public void ListarCliente(int dni) {
		int i=0;
		Cliente c=BuscarClienteXdni(dni);
		if (c!=null) {
			System.out.println("CLIENTE\nNombre y Apellido: "+c.getNmobreApellido()+"\nDNI: "+c.getDni()+"\nMonotributista: "+c.getMonotributista());
			System.out.println("CUENTAS");
			while(i<arreglocuentas.length) {
				if(arreglocuentas[i].getCliente()==c) {
					System.out.println("Numero: "+arreglocuentas[i].getNroCuenta()+"\nSaldo: "+arreglocuentas[i].getSaldo());
					arreglocuentas[i].ListarMovimientos();
					System.out.println("**********************************");
				}
				i++;
			}
			System.out.println("");
		}
		else {
			System.out.println("Cliente no existe");
		}
	}
	
	public void CerrarCuenta(int dni, int nroCuenta) {
		if (BuscarClienteXdni(dni)!=null) {	
				if (arreglocuentas[nroCuenta-1].getCliente()!=null && arreglocuentas[nroCuenta-1].getCliente().getDni()==dni) {
					arreglocuentas[nroCuenta-1].CerrarCuenta();
				}
				else {
					System.out.println("Cuenta Inexistente o Inpropia");
				}
		}
		else {
			System.out.println("Cliente no existe");
		}
	}
	
	public void Depositar(int dni, int nrocuenta, double deposito) {
		if (BuscarClienteXdni(dni)!=null) {
			if (arreglocuentas[nrocuenta-1].getCliente()!=null) {	
				arreglocuentas[nrocuenta-1].Depositar(deposito);
			}
			else {
				System.out.println("Cuenta Inexistente");
			}
		}
		else {
			System.out.println("No existe cliente");
		}
	}
	
	public void RetirarDinero(int dni, int cuenta, double montoAretirar) {
		if (BuscarClienteXdni(dni)!=null) {	
			if (arreglocuentas[cuenta-1].getCliente().getDni()==dni) {
				if (arreglocuentas[cuenta-1].getSaldo()>=montoAretirar) {
					arreglocuentas[cuenta-1].Retiro(montoAretirar);
				}
				else {
					System.out.println("Operacion Invalida, el saldo de tu cuenta es insuficiente.");
				}
			}
			else {
				System.out.println("Operacion Invalida, cuenta no propia.");
			}
		}
		else {
			System.out.println("No es Cliente del Banco.");
		}
	}
	
	public void TransferirDinero(int dni, double montoAtransf, int cuentaOrigen, int cuentaDestino) {
		if (BuscarClienteXdni(dni)!=null) {
			if (arreglocuentas[cuentaOrigen-1].getCliente().getDni()==dni) {
				if (arreglocuentas[cuentaOrigen-1].getSaldo()>=montoAtransf) {
					if (arreglocuentas[cuentaDestino-1].getCliente()!=null) {
						arreglocuentas[cuentaOrigen-1].ActualizarMontoXTransferencia(montoAtransf);
						arreglocuentas[cuentaDestino-1].Transferencia(montoAtransf, dni);
					}
					else {
						System.out.println("La Cuenta destino no existe.");
					}
				}
				else {
					System.out.println("Operacion Invalida, el saldo de tu cuenta es insuficiente.");
				}
			}
			else {
				System.out.println("Operacion Invalida, cuenta no propia.");
			}
		}
		else {
			System.out.println("No es Cliente del Banco.");
		}
	}
}
