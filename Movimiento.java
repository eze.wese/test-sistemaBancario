
public class Movimiento {
	private double monto;
	private int tipo;
	private String operacion;
	Movimiento (double m, int t, String o){
		monto=m;
		tipo=t;
		operacion=o;
	}
	public int getTipo() {
		return tipo;
	}
	public String getOperacion() {
		return operacion;
	}
	public double getMonto() {
		return monto;
	}
}
