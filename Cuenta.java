
public class Cuenta {
	final static int MAX = 10;
	private int nroCuenta;
	private double saldo;
	private Cliente Client;
	Movimiento arregloMovs[];
	Cuenta (int x) {
		nroCuenta=x;
		saldo=0;
		arregloMovs = new Movimiento[MAX];
	}
	
	public Cliente getCliente() {
		return Client;
	}
	public int getNroCuenta() {
		return nroCuenta;
	}
	public double getSaldo() {
		return saldo;
	}
	
	public void setCliente (Cliente c) {
		Client=c;
	}
	
	private void Corrimiento() {
		for (int i=MAX-1; i>0; i--) {
			arregloMovs[i] = arregloMovs[i-1];
		}
	}
	
	public void CerrarCuenta() {
		saldo=0;
		Client=null;
		for (int i=0; i<MAX; i++) {
			arregloMovs[i]=null;
		}
	}
	
	public void Depositar(double deposito) {
		saldo += deposito;
		Corrimiento();
		arregloMovs[0] = new Movimiento(deposito, 1, "deposito");
		if (Client.getMonotributista()==true) {
			double iibb = ((deposito*2)/100);
			saldo -= iibb;
			Corrimiento();
			arregloMovs[0] = new Movimiento(-iibb, 3, "operacion iibb");
		}
	}
	
	public void ActualizarMontoXTransferencia(double x) {
		saldo -= x;
		Corrimiento();
		arregloMovs[0] = new Movimiento(-x, 4, "egreso por transferencia");
	}
	
	public void Transferencia(double x, int dni) {
		saldo += x;
		Corrimiento();
		arregloMovs[0] = new Movimiento(x, 2, "ingreso por transferencia");
		if (Client.getDni()!=dni && Client.getMonotributista()==true) {
			double iibb = ((x*2)/100);
			saldo -= iibb;
			Corrimiento();
			arregloMovs[0] = new Movimiento(-iibb, 3, "iibb");
		}
	}
	
	public void Retiro(double x) {
		saldo -= x;
		Corrimiento();
		arregloMovs[0] = new Movimiento(-x, 5, "Retiro");
	}
	
	public void ListarMovimientos() {
		System.out.println("MOVIMIENTOS");
		int i=0;
		while (arregloMovs[i]!=null && i<arregloMovs.length) {
			System.out.println("Tipo: "+arregloMovs[i].getTipo()+" | Operacion: "+arregloMovs[i].getOperacion()+" | Monto: "+arregloMovs[i].getMonto());
			i++;
		}
	}
}
